package gaspar.ifsc.bloconotas;

import android.content.Context;
import android.content.SharedPreferences;

public class Notas {

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    public Notas(Context c){
        sharedPreferences = c.getSharedPreferences("NOTAS", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();


    }

    public void salvarNota(String s){
        editor.putString("nota", s);
        editor.commit();

    }

    public String recuperaNota(){

        return sharedPreferences.getString("nota", "");
    }
}
